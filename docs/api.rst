API Reference
=============
.. toctree::
   :maxdepth: 1

   apidoc/graphdot.graph
   apidoc/graphdot.kernel
