#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .marginalized import MarginalizedGraphKernel

__all__ = ['MarginalizedGraphKernel']
